package com.pavel.ponomarenko.vkkotlinapp.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavel.ponomarenko.vkkotlinapp.R
import com.pavel.ponomarenko.vkkotlinapp.model.FriendModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_friend.view.*

class FriendsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var friendsList: ArrayList<FriendModel> = ArrayList()
    var sourceList: ArrayList<FriendModel> = ArrayList()

    fun setupFriends(friendList: ArrayList<FriendModel>) {
        sourceList.clear()
        sourceList.addAll(friendList)
        filter(text = "")
    }

    fun filter(text: String) {
        friendsList.clear()
        sourceList.forEach{
            if (it.name.contains(text, ignoreCase = true) || it.surname.contains(text, ignoreCase = true)) {
                friendsList.add(it)
            } else {
                it.city?.let { city -> if (city.contains(text, ignoreCase = true))
                    friendsList.add(it)
                }
            }
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, view: Int): RecyclerView.ViewHolder =
        FriendsViewHolder(itemView = LayoutInflater.from(parent.context).inflate(R.layout.cell_friend, parent, false))

    override fun getItemCount(): Int = friendsList.count()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FriendsViewHolder) {
            holder.bind(friendModel = friendsList[position])
        }
    }

    class FriendsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(friendModel: FriendModel) {

            friendModel.avatar?.let { Picasso.with(itemView.context).load(it).into(itemView.friendProfileImage) }
                ?: Picasso.with(itemView.context).load(R.drawable.ic_launcher_foreground).into(itemView.friendProfileImage)

            itemView.friendName.text = "${friendModel.name} ${friendModel.surname}"
            friendModel.city?.let { itemView.friendCity.text = it }
                ?: itemView.friendCity.setText(itemView.context.getString(R.string.city_error))

            if (friendModel.isOnline) itemView.friendImgOnline.visibility =
                View.VISIBLE else itemView.friendImgOnline.visibility = View.GONE
        }
    }
}