package com.pavel.ponomarenko.vkkotlinapp.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pavel.ponomarenko.vkkotlinapp.R
import com.pavel.ponomarenko.vkkotlinapp.model.FriendModel
import com.pavel.ponomarenko.vkkotlinapp.providers.FriendsProvider
import com.pavel.ponomarenko.vkkotlinapp.views.FriendsView

@InjectViewState
class FriendsPresenter : MvpPresenter<FriendsView>() {
    fun loadFriends() {
        viewState.startLoading()
        FriendsProvider(presenter = this).loadFriend()
    }

    fun friendsLoaded(friendList: ArrayList<FriendModel>) {
        viewState.endLoading()
        if (friendList.size == 0) {
            viewState.setupEmptyList()
            viewState.showError(textResource = R.string.emptyFriendsList)
        } else {
            viewState.setupFriendsList(friendList = friendList)
        }
    }

    fun showError(textResource: Int) {
        viewState.showError(textResource = textResource)
    }
}