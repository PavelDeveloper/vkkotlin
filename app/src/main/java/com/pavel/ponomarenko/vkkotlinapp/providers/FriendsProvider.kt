package com.pavel.ponomarenko.vkkotlinapp.providers

import android.os.Handler
import com.google.gson.JsonParser
import com.pavel.ponomarenko.vkkotlinapp.R
import com.pavel.ponomarenko.vkkotlinapp.model.FriendModel
import com.pavel.ponomarenko.vkkotlinapp.presenters.FriendsPresenter
import com.vk.sdk.api.*

class FriendsProvider(var presenter: FriendsPresenter) {

    fun testLoadFriends(hasFriends: Boolean) {
        Handler().postDelayed({
            var friendList: ArrayList<FriendModel> = ArrayList()
            if (hasFriends) {
                friendList = arrayListOf(
                    FriendModel(
                        name = "ivan",
                        surname = "ivanov",
                        city = null,
                        avatar = "https://www.bigstockphoto.com/images/homepage/module-4.jpg",
                        isOnline = true
                    ),
                    FriendModel(
                        name = "egor",
                        surname = "demid",
                        city = "Don",
                        avatar = "https://picjumbo.com/wp-content/uploads/alone-with-his-thoughts-1080x720.jpg",
                        isOnline = true
                    )
                )
            } else {
                friendList.clear()
            }
            presenter.friendsLoaded(friendList = friendList)
        }, 2000)
    }

    fun loadFriend() {
        val request = VKApi.friends()
            .get(
                VKParameters.from(
                    VKApiConst.COUNT,
                    100,
                    VKApiConst.FIELDS,
                    "sex,bdate,city, country, photo_100, online"
                )
            )
        request.executeWithListener(object : VKRequest.VKRequestListener() {
            override fun onComplete(response: VKResponse?) {
                super.onComplete(response)

                var friendList: ArrayList<FriendModel> = ArrayList()

                val jsonParser = JsonParser()
                val parsedJson = jsonParser.parse(response?.json.toString()).asJsonObject
                parsedJson.get("response").asJsonObject.getAsJsonArray("items").forEach {
                    val friend = FriendModel(
                        name = it.asJsonObject.get("first_name").asString,
                        surname = it.asJsonObject.get("last_name").asString,
                        city = it.asJsonObject.get("city")?.asJsonObject?.get("title")?.asString ?: "No city",
                        avatar = it.asJsonObject.get("photo_100").asString,
                        isOnline = it.asJsonObject.get("online").asInt == 1
                    )
                    friendList.add(friend)
                }
                presenter.friendsLoaded(friendList = friendList)
            }
            override fun onError(error: VKError?) {
                super.onError(error)
                presenter.showError(textResource = R.string.error_friends_loading)
            }
        })
    }
}