package com.pavel.ponomarenko.vkkotlinapp.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavel.ponomarenko.vkkotlinapp.R
import com.pavel.ponomarenko.vkkotlinapp.adapters.FriendsAdapter
import com.pavel.ponomarenko.vkkotlinapp.model.FriendModel
import com.pavel.ponomarenko.vkkotlinapp.presenters.FriendsPresenter
import com.pavel.ponomarenko.vkkotlinapp.views.FriendsView
import kotlinx.android.synthetic.main.activity_friends.*

class FriendsActivity : MvpAppCompatActivity(), FriendsView {

    @InjectPresenter
    lateinit var friendsPresenter: FriendsPresenter

    private lateinit var friendsAdapter: FriendsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        friendsPresenter.loadFriends()

        friendEditText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                friendsAdapter.filter(text = s.toString())
            }
        })

        friendsAdapter = FriendsAdapter()

        recyclerFriends.apply {
            adapter = friendsAdapter
            layoutManager = LinearLayoutManager(applicationContext, OrientationHelper.VERTICAL, false)
            setHasFixedSize(true)
        }
    }

    override fun startLoading() {
        circularProgressFriends.visibility = View.VISIBLE
        recyclerFriends.visibility = View.GONE
        txvError.visibility = View.GONE

    }

    override fun endLoading() {
        circularProgressFriends.visibility = View.GONE
    }

    override fun showError(textResource: Int) {
        txvError.text = getString(textResource)
    }

    override fun setupEmptyList() {
        recyclerFriends.visibility = View.GONE
        txvError.visibility = View.VISIBLE
    }

    override fun setupFriendsList(friendList: ArrayList<FriendModel>) {
        recyclerFriends.visibility = View.VISIBLE
        txvError.visibility = View.GONE

        friendsAdapter.setupFriends(friendList = friendList)
    }
}
