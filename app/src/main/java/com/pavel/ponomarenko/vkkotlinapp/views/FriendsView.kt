package com.pavel.ponomarenko.vkkotlinapp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pavel.ponomarenko.vkkotlinapp.model.FriendModel

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface FriendsView: MvpView {
    fun startLoading()
    fun endLoading()
    fun showError(textResource: Int)
    fun setupEmptyList()
    fun setupFriendsList(friendList: ArrayList<FriendModel>)
}