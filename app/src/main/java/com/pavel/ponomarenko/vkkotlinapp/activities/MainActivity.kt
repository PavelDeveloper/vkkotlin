package com.pavel.ponomarenko.vkkotlinapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavel.ponomarenko.vkkotlinapp.R
import com.pavel.ponomarenko.vkkotlinapp.presenters.LoginPresenter
import com.pavel.ponomarenko.vkkotlinapp.views.LoginView
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity(), LoginView {

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //enterButton.setOnClickListener { loginPresenter.login(isSuccess = true) }
        enterButton.setOnClickListener { VKSdk.login(this, VKScope.FRIENDS) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       if (!loginPresenter.login(requestCode = requestCode, resultCode = resultCode, data = data))
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun startLoading() {
        enterButton.visibility = View.GONE
        progress_view.visibility = View.VISIBLE
    }

    override fun endLoading() {
        progress_view.visibility = View.GONE
        enterButton.visibility = View.VISIBLE
    }

    override fun openFriends() {
        startActivity(Intent(applicationContext, FriendsActivity::class.java))
    }

    override fun showError(textResource: Int) {
        Toast.makeText(applicationContext, getString(textResource), Toast.LENGTH_LONG).show()
    }
}
